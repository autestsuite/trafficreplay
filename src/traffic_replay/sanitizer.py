'''
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
'''

import json
import os
import sys
import time
import multiprocessing
from queue import Queue, Empty
from threading import Thread, Barrier
import argparse
from copy import deepcopy
# Default template replay file with basic elements
template = json.loads('{"meta": {"version":"1.0"},"sessions":[]}')
verbose = 0

def insertUUID(transactions):
    for transaction in transactions:
        id_string = transaction["uuid"]
        if "client-request" in transaction:
            transaction["client-request"]["headers"]["fields"].append(
                ["uuid", id_string])
        if "proxy-request" in transaction:
            transaction["proxy-request"]["headers"]["fields"].append(
                ["uuid", id_string])
# Read in every n files in indir and combine into one json object
# Put the resulting object into out_q for later writing

def verifyReq(request):
    """ Function to verify request with method, url, and headers
    Args:
        request (json object)
    Returns:
        True for valid request.
    """
    if not request:
        return False
    if not request["method"]:
        return False
    if not request["url"]:
        return False
    if not request["headers"]:
        return False
    return True

def verifyResp(response):
    """ Function to verify response with status
    Args:
        response (json object)
    Returns:
        True for valid response.
    """
    if not response:

        return False
    if not response["status"]:
        return False
    return True

def verifyTxn(transaction):
    """ Function to verify transaction.
        A valid transaction might contain either 1) four stage or
        2) only two of them due to cache.
    Args:
        transaction (json object)
    Returns:
        True for valid transaction.
    """
    if not transaction:
        return False
    if not verifyReq(transaction["client-request"]):
        return False
    if not verifyResp(transaction["proxy-response"]):
        return False

    if transaction["proxy-request"]:
        if not verifyReq(transaction["proxy-request"]):
            return False
        if not verifyResp(transaction["server-response"]):
            return False
    return True


def verifySsn(session):
    """ Function to verify session.
        A valid session contains a valid list of transactions.
    Args:
        transaction (json object)
    Returns:
        True for valid transaction.
    """
    if not session:
        return False
    if not session["transactions"]:
        if verbose > 1:
            print("Empty transaction list")
        return False
    for txn in session["transactions"]:
        if not verifyTxn(txn):
            return False
    return True

def writeSessions(sessions, filename):
    smoothie = deepcopy(template)
    smoothie["sessions"] = deepcopy(sessions)
    for session in smoothie["sessions"]:
        insertUUID(session["transactions"])
    with open(filename, "w") as f:
        json.dump(smoothie, f, ensure_ascii=False)
        if verbose > 0:
            print("{} has {} sessions".format(filename, len(sessions)))

def readAndCombine(in_dir, sub_dir, n, out_dir):
    """ Function to read raw dump files, sanitize and combine to generate output files
    Args:
        in_dir (string) Full path to dumps
        sub_dir (string) Used to generate output filename
        n (int) number of sessions in each output file
        out_q (Queue) Output queue
    """
    count = 0
    batch_count = 0
    txn_count = 0
    smoothie = deepcopy(template)
    sessions = []
    for f in os.listdir(in_dir):
        if os.path.isfile(os.path.join(in_dir, f)):
            try:
                fd = open('{}/{}'.format(in_dir, f), 'r')
                try:
                    data = json.load(fd)
                except Exception as e:
                    if verbose > 1:
                        print(
                            "Failed to load {}/{} as a json object. error: {}".format(in_dir, f, e))
                    fd.close()
                    continue
                # Do checks to verify the session
                for session in data["sessions"]:
                    if verifySsn(session):
                        sessions.append(session)
                        count += 1
                        txn_count += len(session["transactions"])
                if len(sessions) == n:
                    writeSessions(sessions, "{}/{}_{}.json".format(out_dir, sub_dir, batch_count))
                    # smoothie["sessions"] = deepcopy(sessions)
                    # for session in smoothie["sessions"]:
                    #     insertUUID(session["transactions"])
                    # with open("{}/{}_{}.json".format(out_dir, sub_dir, batch_count), "w") as f:
                    #     json.dump(smoothie, f, ensure_ascii=False)
                    #     print("{}/{}_{}.json has {} sessions".format(out_dir,sub_dir,int(count/n),len(sessions)))
                    # smoothie = deepcopy(template)
                    sessions = []
                    batch_count += 1
            except Exception as e:
                if verbose > 1:
                    print("Failed to handle {}/{}. ERROR: {}".format(in_dir, f, e))
                continue
    if sessions:
        writeSessions(sessions, "{}/{}_{}.json".format(out_dir, sub_dir, batch_count))
        # smoothie["sessions"] = deepcopy(sessions)
        # for session in smoothie["sessions"]:
        #     insertUUID(session["transactions"])
        # with open("{}/{}_{}.json".format(out_dir, sub_dir, batch_count), "w") as f:
        #     json.dump(smoothie, f, ensure_ascii=False)
        #     print("{}/{}_{}.json has {} sessions".format(out_dir,sub_dir,batch_count,len(sessions)))
    #print('{} has {} sessions and {} transactions'.format(in_dir, count, txn_count))

    return count, txn_count


def writeToFile(out_dir, out_q):
    """ Function for writing to disk
    Args:
        out_dir (string) Path to output dir
        out_q (Queue) Output queue
    """
    while not out_q.empty():
        try:
            out_data = out_q.get(False)
        except Empty:
            break
        with open("{}/{}".format(out_dir, out_data[0]), "w", encoding="ascii", errors="surrogateescape") as f:
            json.dump(out_data[1], f, indent=4)


def sanitize(in_dir, subdir_q, out_dir, n, out_q, cnt_q, barrier):
    """ Function used to set up individual threads
    Args:
        in_dir (string) Path to dumps directory
        subdir_q (Queue) Queue of subdir to read from
        out_dir (string) Path to output dir
        n (int) Number of sessions per file
        out_q (Queue) Output queue
        cnt_q (Queue) Ssn and txn count queue
    """
    while not subdir_q.empty():
        subdir = subdir_q.get()
        cnt = readAndCombine(os.path.join(
            in_dir, subdir), subdir, n, out_dir)
        cnt_q.put(cnt)
    writeToFile(out_dir, out_q)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", type=str, dest='in_dir',
                        help="Input directory of log files (from traffic_dump)")
    parser.add_argument("-o", type=str, dest='out_dir',
                        help="Output directory of replay files")
    parser.add_argument("-n", type=int, dest='sessions',
                        default=10, help="Number of sessions in one output file")
    parser.add_argument("-v", type=int, dest='verbose', default=0, help="verbose levels")

    args = parser.parse_args()
    verbose = args.verbose

    # generate thread arguments
    subdir_list = []
    out_q = Queue()
    subdir_q = Queue()
    cnt_q = Queue()
    for subdir in os.listdir(args.in_dir):
        if os.path.isdir(os.path.join(args.in_dir, subdir)):
            subdir_q.put(subdir)

    threads = []
    barrier = Barrier(max(len(subdir_list), 1), timeout=20)
    nthreads = min(max(subdir_q.qsize(), 1), 32)

    # start up threads
    for i in range(nthreads):
        t = Thread(target=sanitize, args=(args.in_dir, subdir_q,
                                          args.out_dir, args.sessions, out_q, cnt_q, barrier))
        t.start()
        threads.append(t)
    for t in threads:
        t.join()

    # retrieve count
    ssn = 0
    txn = 0
    for elem in list(cnt_q.queue):
        ssn += elem[0]
        txn += elem[1]
    print("Total {} sessions and {} transactions.".format(ssn, txn))
    with open("{}/summary.txt".format(args.out_dir), "w", encoding="ascii") as f:
        f.write("Total {} sessions and {} transactions.".format(ssn, txn))


